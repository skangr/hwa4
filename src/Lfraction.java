import java.util.*;


/**
 * This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

    /**
     * Main method. Different tests.
     */
    public static void main(String[] param) {
    }

    private long numerator = 0;
    private long denominator = 1;


    /**
     * Constructor.
     *
     * @param a numerator
     * @param b denominator > 0
     */
    public Lfraction(long a, long b) {
        if (b == 0) {
            throw new RuntimeException("Fraction denominator cannot be 0");
        }
        this.numerator = a;
        this.denominator = b;
        reduceFraction();
    }

    /**
     * Public method to access the numerator field.
     *
     * @return numerator
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Public method to access the denominator field.
     *
     * @return denominator
     */
    public long getDenominator() {
        return denominator; //
    }

    /**
     * Conversion to string.
     *
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
        return getNumerator() + "/" + getDenominator();
    }

    /**
     * Equality test.
     *
     * @param m second fraction
     * @return true if fractions this and m are equal
     */

    @Override
    public boolean equals(Object m) {
        if (!(m instanceof Lfraction)) {
            throw new IllegalArgumentException(m + "Isn't an Lfraction type so comparison cannot be made");
        }
        reduceFraction();
        ((Lfraction) m).reduceFraction();
        return getNumerator() == ((Lfraction) m).getNumerator() && getDenominator() == ((Lfraction) m).getDenominator();
    }

    /**
     * Hashcode has to be the same for equal fractions and in general, different
     * for different fractions.
     * https://www.baeldung.com/java-objects-hash-vs-objects-hashcode
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(getNumerator(), getDenominator());
    }


    private void reduceFraction() {
        if (getNumerator() == 0) {
            denominator = 1;
            return;
        }
        //it should either use numerator or denominator to simply the fraction, whatever is smaller
        long smallerNumber = Math.min(Math.abs(getNumerator()), Math.abs(getDenominator()));

        for (long i = smallerNumber + 1; i > 1; i--) {
            if (getNumerator() % i == 0 && getDenominator() % i == 0) {
                denominator /= i;
                numerator /= i;
            }
        }
        if (getNumerator() < 0 && getDenominator() < 0) {
            denominator *= -1;
            numerator *= -1;
        }
    }

    /**
     * Sum of fractions.
     *
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus(Lfraction m) {

        Lfraction result = new Lfraction(getNumerator() * m.getDenominator() + m.getNumerator() * getDenominator(), getDenominator() * m.getDenominator());
        result.reduceFraction();

        return result;
    }


    /**
     * Multiplication of fractions.
     *
     * @param m second factor
     * @return this*m
     */
    public Lfraction times(Lfraction m) {
        if (m.getNumerator() == 0 || getNumerator() == 0) {
            return new Lfraction(0, 1);
        }
        Lfraction result = new Lfraction(getNumerator() * m.getNumerator(), getDenominator() * m.getDenominator());
        result.reduceFraction();
        return result;

    }

    /**
     * Inverse of the fraction. n/d becomes d/n.
     *
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
        if (getNumerator() == 0) {
            throw new RuntimeException("Cannot reverse fraction with 0 for numerator");
        }
        int correction = 1;
        if (getNumerator() < 0) {
            correction = -1;
        }
        return new Lfraction(getDenominator() * correction, getNumerator() * correction);
    }

    /**
     * Opposite of the fraction. n/d becomes -n/d.
     *
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
        return new Lfraction(getNumerator() * (-1), getDenominator());
    }

    /**
     * Difference of fractions.
     *
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus(Lfraction m) {
        Lfraction result = new Lfraction(getNumerator() * m.getDenominator() - m.getNumerator() * getDenominator(), getDenominator() * m.getDenominator());
        result.reduceFraction();

        return result;
    }

    /**
     * Quotient of fractions.
     *
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy(Lfraction m) {
        if (m.getNumerator() == 0) {
            throw new RuntimeException("Cannot divide by fraction with 0 in numerator");
        }
        return times(m.inverse());
    }

    /**
     * Comparision of fractions.
     *
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo(Lfraction m) {
        if (equals(m)) {
            return 0;
        }
        if (getNumerator() * m.getDenominator() > m.getNumerator() * getDenominator()) {
            return 1;
        }
        return -1;
    }

    /**
     * Clone of the fraction.
     *
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        try {
            return new Lfraction(getNumerator(), getDenominator());
        } catch (Exception e) {
            throw new CloneNotSupportedException();
        }

    }

    /**
     * Integer part of the (improper) fraction.
     *
     * @return integer part of this fraction
     */
    public long integerPart() {
        return getNumerator() / getDenominator();
    }

    /**
     * Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     *
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
        Lfraction result = new Lfraction(getNumerator() - (integerPart() * getDenominator()), denominator);
        reduceFraction();
        return result;
    }

    /**
     * Approximate value of the fraction.
     *
     * @return real value of this fraction
     */
    public double toDouble() {
        return (double) getNumerator() / getDenominator();
    }

    /**
     * Double value f presented as a fraction with denominator d > 0.
     *
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction(double f, long d) {
        long newNumerator = Math.round(f * d);
        return new Lfraction(newNumerator, d);
    }

    public Lfraction pow(int a) {
        Lfraction result = null;
        if (a == 0) {
            result = new Lfraction(1, 1);
            System.out.println(result);
        } else if (a == 1) {
            result = new Lfraction(getNumerator(), getDenominator());
        } else if (a == -1) {
            result = inverse();
        } else if (a > 1) {
            result = times(pow(a - 1));
        } else {
            result = pow(Math.abs(a)).inverse();
        }
        return result;
    }

    /**
     * Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     *
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf(String s) {
        String[] array = s.split("/", 2);
        if (array.length != 2) {
            throw new RuntimeException("String " + s + " isn't valid fraction");
        }
        long newNumerator;
        long newDenominator;
        try {
            newNumerator = Long.parseLong(array[0]);
            newDenominator = Long.parseLong(array[1]);
        } catch (Exception e) {
            throw new RuntimeException("String " + s + " isn't valid fraction");
        }
        return new Lfraction(newNumerator, newDenominator);
    }
}

